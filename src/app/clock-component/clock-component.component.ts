import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-clock-component',
  templateUrl: './clock-component.component.html',
  styleUrls: ['./clock-component.component.scss'],
})

export class ClockComponentComponent implements OnInit {

  private curentTimeInterval;
  private exactTime = true;
  private hours;
  private minutes;
  private seconds;
  private timeAngles = {
    minutesAngle: `rotate(0deg)`,
    secondsAngle: `rotate(0deg)`,
    hoursAngle: `rotate(0deg)`
  };
  private timeSet = {
    minutesSet: 0,
    secondsSet: 0,
    hoursSet: 0
  };
  private intervalCounter = 0;

  @Input() set typeOfClock(res) {
    if (res === 'exactTime') {
      this.exactTime = true;
    } else {
      this.exactTime = false;
    }
  }

  ngOnInit() {
    if (this.exactTime ) {
      this.startCurrentTimeInterval();
    } else {
      this.exactTime = false;
    }
  }

  startCurrentTimeInterval() {
    const vm = this;

    this.curentTimeInterval = setInterval(function () {
      const now = new Date().getTime();
      vm.hours = Math.floor((now % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      vm.seconds = Math.floor((now % (1000 * 60)) / 1000);
      vm.minutes = Math.floor((now % (1000 * 60 * 60)) / (1000 * 60));

      if (vm.intervalCounter === 0) {
        vm.timeAngles.minutesAngle = `rotate(${vm.minutes * 6}deg)`;
        vm.timeAngles.hoursAngle = `rotate(${vm.hours * 30}deg)`;
      }
      vm.intervalCounter++;

      vm.timeAngles.secondsAngle = `rotate(${vm.seconds * 6}deg)`;
      if (vm.seconds % 60 === 0) {
        vm.timeAngles.minutesAngle = `rotate(${vm.minutes * 6}deg)`;
      }
      if (vm.minutes % 60 === 0) {
        vm.timeAngles.hoursAngle = `rotate(${vm.hours * 30}deg)`;
      }
    }, 1000);
  }

  buttonClicked() {
    this.startCurrentTimeInterval();
  }

  buttonStopClicked() {
    clearInterval(this.curentTimeInterval);
  }

  setClock() {
    const vm = this;

    if (vm.timeSet.hoursSet >= 0 && vm.timeSet.hoursSet <= 12) {
      vm.timeAngles.hoursAngle = `rotate(${vm.timeSet.hoursSet * 30}deg)`;
    }

    if (vm.timeSet.minutesSet >= 0 && vm.timeSet.minutesSet < 60) {
      vm.timeAngles.minutesAngle = `rotate(${vm.timeSet.minutesSet * 6}deg)`;
    }

    if (vm.timeSet.secondsSet >= 0 && vm.timeSet.secondsSet < 60) {
      vm.timeAngles.secondsAngle = `rotate(${vm.timeSet.secondsSet * 6}deg)`;
    }

  }

  resetClock() {
    const vm = this;
      vm.timeAngles.hoursAngle = `rotate(0deg)`;
      vm.timeAngles.minutesAngle = `rotate(0deg)`;
      vm.timeAngles.secondsAngle = `rotate(0deg)`;

      vm.timeSet = {
        minutesSet: 0,
        secondsSet: 0,
        hoursSet: 0
      };
  }

}
